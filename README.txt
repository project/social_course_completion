# Social course completion

This module will install a new field type called "CourseAttemptManagement". It extends the Course Interface, Social course controller and Course Wrapper.

## Dependencies

- drupal:social_course
- drupal:field

## Requirement Information

Issue: https://www.drupal.org/project/drupal/issues/937442. To uninstall this module you need to use the following drush command due to an issue. drush config:delete field.storage.node.field_attempt_management

## Configuration

There is no configuration for this module. Just install it like any other drupal module.
