<?php

namespace Drupal\social_course_completion;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Replace social course course wrapper service with our own.
 */
class SocialCourseCompletionServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('social_course.course_wrapper');
    $definition->setClass('\Drupal\social_course_completion\SocialCourseCompletionCourseWrapper');
  }
}