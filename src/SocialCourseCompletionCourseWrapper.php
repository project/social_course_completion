<?php

namespace Drupal\social_course_completion;

use Drupal\social_course\CourseWrapper;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\group\Entity\GroupContent;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\social_course\Entity\CourseEnrollmentInterface;
use Drupal\social_course\Entity\CourseEnrollment;
use Drupal\social_course_completion\CourseEnrollmentCompletionInterface;
use Drupal\social_course_completion\CourseCompletionInterface;
use Symfony\Component\Validator\Exception\InvalidArgumentException;
use Drupal\Core\Url;

/**
 * Replace social course course wrapper class with our own.
 */
class SocialCourseCompletionCourseWrapper extends CourseWrapper {

  /**
   * {@inheritdoc}
   */
  public function getCourseStatus(AccountInterface $account) {
    $storage = $this->entityTypeManager->getStorage('course_enrollment');
    $entities = $storage->loadByProperties([
      'uid' => $account->id(),
      'gid' => $this->getCourse()->id(),
    ]);

    if (!$entities) {
      return CourseEnrollmentInterface::NOT_STARTED;
    }

    foreach ($entities as $entity) {
      if ($entity->getStatus() === CourseEnrollmentInterface::IN_PROGRESS) {
        return CourseEnrollmentInterface::IN_PROGRESS;
      }
      if ($entity->getStatus() === CourseEnrollmentCompletionInterface::FAILED) {
        return CourseEnrollmentCompletionInterface::FAILED;
      }
    }

    return CourseEnrollmentInterface::FINISHED;
  }

  /**
   * {@inheritdoc}
   */
  public function getSectionStatus(NodeInterface $node, AccountInterface $account) {
    $storage = $this->entityTypeManager->getStorage('course_enrollment');
    $entities = $storage->loadByProperties([
      'uid' => $account->id(),
      'gid' => $this->getCourse()->id(),
      'sid' => $node->id(),
    ]);

    if (!$entities) {
      return CourseEnrollmentInterface::NOT_STARTED;
    }

    foreach ($entities as $entity) {
      /** @var \Drupal\social_course\Entity\CourseEnrollmentInterface $entity */
      if ($entity->getStatus() === CourseEnrollmentInterface::IN_PROGRESS) {
        return CourseEnrollmentInterface::IN_PROGRESS;
      }
      if ($entity->getStatus() === CourseEnrollmentCompletionInterface::FAILED) {
        return CourseEnrollmentCompletionInterface::FAILED;
      }
    }

    return CourseEnrollmentInterface::FINISHED;
  }

  /**
   * {@inheritdoc}
   */
  public function isCourseEnrollment($conditions) {

    if (is_array($conditions) && !empty($conditions)) {
      $storage = $this->entityTypeManager->getStorage('course_enrollment');
      $entities = $storage->loadByProperties($conditions);
      if ($entity = array_pop($entities)) {
        return $entity->id();
      }
    }

    return FALSE;

  }

  /**
   * Saves course completion
   *
   * @param int $course_enrollment_id
   *  Id of the course enrollment
   * @param array $score_data
   *  The score data
   */
  public function saveCourseCompletion(int $course_enrollment_id, array $score_data) {

    if(is_numeric($course_enrollment_id) &&
       is_array($score_data) && !empty($score_data)) {
      $storage = $this->entityTypeManager->getStorage('course_completion');
      $entities = $storage->loadByProperties([
        'cid' => $course_enrollment_id
      ]);
      if (!$entities) {
        $storage->create([
          'cid' => $course_enrollment_id,
          'score_raw' => $score_data['raw'],
          'score_min' => $score_data['min'],
          'score_max' => $score_data['max'],
          'status' => CourseCompletionInterface::COMPLETED,
        ])->save();
      }

      // Update the course enrollment status
      $storage = $this->entityTypeManager->getStorage('course_enrollment');
      $course_enrollment = $storage->load($course_enrollment_id);
      $course_enrollment->setStatus(CourseEnrollmentInterface::FINISHED);
      $course_enrollment->save();

    }
  }

  /**
   * Saves all course attempts
   *
   * @param int $course_enrollment_id
   *  Id of the course enrollment
   * @param \Drupal\node\NodeInterface $node
   *  Node object
   * @param array $score_data
   *  Array of scores
   */
  public function saveCourseAttempt(int $course_enrollment_id, NodeInterface $node, array $score_data) {

    if(is_numeric($course_enrollment_id)
       && isset($node) && isset($score_data) && !empty($score_data)) {
      // Check if we have an attempt managemnt field
      // and check if we are on a self scoring material type
      if ($node->hasField('field_attempt_management') && isset($node->field_attempt_management) &&
          in_array($node->bundle(),_social_course_completion_get_self_scoring_material_types())) {
        // Read the field_attempt_management array
        // as It's always limited to one record get the first one

        $attempt_management = $node->field_attempt_management->getValue()[0];
        $allowed_attempts = $attempt_management['allowed_attempts'];
        $attempt_rule = $attempt_management['attempt_rule'];

        $storage = $this->entityTypeManager->getStorage('course_attempts');
        $entities = $storage->loadByProperties([
          'cid' => $course_enrollment_id,
          'uid' => \Drupal::currentUser()->id(),
        ]);
        if (!$entities) {
          // Create the attempt
          $storage->create([
            'cid' => $course_enrollment_id,
            'score_raw' => $score_data['raw'],
            'score_min' => $score_data['min'],
            'score_max' => $score_data['max'],
          ])->save();
        }
        else {
          // as we already have one record we have to check
          // if we have to disallow another attempt
          if (count($entities) < $allowed_attempts || $allowed_attempts == 0) {
            // Create the attempt
            $storage->create([
              'cid' => $course_enrollment_id,
              'score_raw' => $score_data['raw'],
              'score_min' => $score_data['min'],
              'score_max' => $score_data['max'],
            ])->save();
          }
        }

        $storage = $this->entityTypeManager->getStorage('course_attempts');
        $entities = $storage->loadByProperties([
          'cid' => $course_enrollment_id,
          'uid' => \Drupal::currentUser()->id(),
        ]);

        if ($allowed_attempts > 0 && count($entities) == $allowed_attempts) {
          $storage = $this->entityTypeManager->getStorage('course_enrollment');
          $course_enrollment = $storage->load($course_enrollment_id);
          if ($course_enrollment instanceof CourseEnrollment && $course_enrollment->getStatus() !== CourseEnrollmentInterface::FINISHED) {
            // Set the status of FAILED so we can avoid presenting
            // the material again.
            $course_enrollment->setStatus(CourseEnrollmentCompletionInterface::FAILED);
            $course_enrollment->save();
          }
        }        
      }
    }
  }

  /**
   * Checks for self scoring material
   *
   * @param int $material_id
   *  The node id of the material
   * @return bool
   *  TRUE or FALSE
   */
  public function isSelfScoringMaterial(int $material_id) {
    $storage = $this->entityTypeManager->getStorage('node');
    $material = $storage->load($material_id);
    if ($material instanceOf NodeInterface &&
        isset($material->field_attempt_management) &&
        in_array($material->bundle(),_social_course_completion_get_self_scoring_material_types())
    ) {

      return TRUE;

    }

    return FALSE;

  }

  /**
   * Checks if self scoring material has been completed
   *
   * @param integer $course_enrollment_id
   *  The id of the course enrollment table
   * @return boolean
   *  TRUE or FALSE 
   */
  public function isSelfScoringMaterialCompleted(int $course_enrollment_id) {
    $storage = $this->entityTypeManager->getStorage('course_completion');
    $course_completion = $storage->loadByProperties([
      'cid' => $course_enrollment_id,
      'uid' => \Drupal::currentUser()->id(),
    ]);
    if(!$course_completion) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Checks for attempt exceeded materials
   *
   * @param integer $course_enrollment_id
   *  The id of the course enrollment table
   * @param $course_attempt_management
   *  The field course_attempt_management
   * @return boolean
   *  TRUE or FALSE
   */
  public function isSelfScoringMaterialAllowedAttemptsExceeded(int $course_enrollment_id, $course_attempt_management) {
    $storage = $this->entityTypeManager->getStorage('course_attempts');
    $course_attempts = $storage->loadByProperties([
      'cid' => $course_enrollment_id,
      'uid' => \Drupal::currentUser()->id(),
    ]);
    if(!$course_attempts) {
      return TRUE;
    }
    return FALSE;
  }  

  /**
   * {@inheritdoc}
   */  
  public function shuffleNextOpenMaterial(AccountInterface $account, NodeInterface $current_material) {
    $materials = FALSE;
    $next_url = FALSE;
    $storage = $this->entityTypeManager->getStorage('course_enrollment');
    $entities = $storage->loadByProperties([
      'uid' => $account->id(),
      'gid' => $this->getCourse()->id(),
    ]);

    foreach ($entities as $entity) {
      if ($entity->getStatus() !== CourseEnrollmentInterface::FINISHED &&
          $entity->getMaterialId() !== $current_material->id()) {
        $mid = $entity->getMaterialId();
        $materials[] = $mid;
      }
    }

    if ($materials) {
      $count_materials = count($materials);
      $shuffel = random_int(0,$count_materials);
      $randomized_material_url = $this->getNextMaterialUrl(
        $materials,
        $shuffel
      );

      if ($randomized_material_url) {
        $next_url = $randomized_material_url;
      }

    }
    else {
      // If we don't have any material we want unenrolled sections
      $course_enrollment_conditions = [
        'gid' => $this->getCourse()->id(),
        'uid' => $account->id(),
      ];
      $current_section = $this->getSectionFromMaterial($current_material);
      if ($current_section instanceof NodeInterface) {
        $current_section_id = $current_section->id();
        $course_sections = $this->getSections();
        if ($course_sections) {
          foreach ($course_sections as $key => $section) {
            $course_enrollment_conditions['sid'] = $section->id();
            if ($key != $current_section_id &&
                !$this->isCourseEnrollment($course_enrollment_conditions)
            ) {
              $next_section_id = $section->id();
            }
          }
          if (isset($next_section_id)) {
            $next_url = $this->getNextSecitonUrl($next_section_id);
          }
        }
      }
    }

    return $next_url;

  }

  /**
   * Gets the next section url
   *
   * @param int $section_id
   *   The section id (node id) as integer
   *
   * @return \Drupal\Core\Url $next_url
   */
  protected function getNextSecitonUrl(int $section_id) {

    $next_url = FALSE;

    if ($section_id) {
      $next_url = Url::fromRoute('social_course.next_material', [
        'group' => $this->getCourse()->id(),
        'node' => $section_id,
      ], [
        'attributes' => [
          'class' => [
            'btn',
            'btn-raised',
            'btn-primary',
            'waves-effect',
          ],
        ],
      ]);
    }

    return $next_url;

  }

  /**
   * Gets a shuffeled material url
   *
   * @param array $materials
   *   An array of unfinished materials
   * @param $shuffel
   *   The randomized key for the material to pick
   *
   * @return \Drupal\Core\Url $next_url
   *   An URL object of the next material
   */
  protected function getNextMaterialUrl($materials, $shuffel) {

    $next_url = FALSE;

    if ($materials && is_array($materials)) {

      foreach($materials as $key => $material_id) {

        if ($key == $shuffel) {
          $nid = $material_id;
          $node = $this->entityTypeManager
            ->getStorage('node')
            ->load($nid);
          $material = $this->getSectionFromMaterial($node);

          $next_url = Url::fromRoute('node.cannocial', [
            'node' => $section_id,
          ], [
            'attributes' => [
              'class' => [
                'btn',
                'btn-raised',
                'btn-primary',
                'waves-effect',
              ],
            ],
          ]);
        }
      }
    }

    return $next_url;

  }

}

