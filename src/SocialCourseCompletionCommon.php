<?php

namespace Drupal\social_course_completion;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Drupal\social_course_completion\SocialCourseCompletionCourseWrapper;
use Drupal\social_course\Entity\CourseEnrollmentInterface;
use Drupal\social_course_completion\CourseEnrollmentCompletionInterface;

/**
 * SocialCourseCompletionCommon service.
 */
class SocialCourseCompletionCommon {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The social course completion wrapper.
   *
   * @var \Drupal\social_course_completion\SocialCourseCompletionCourseWrapper
   */
  protected $courseWrapper;  

  /**
   * Constructs a SocialCourseCompletionCommon object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\social_course_completion\SocialCourseCompletionCourseWrapper $course_wrapper
   *   The extended course wrapper.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountInterface $current_user, SocialCourseCompletionCourseWrapper $course_wrapper) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
    $this->courseWrapper = $course_wrapper;
  }

  /**
   * Method description.
   */
  public function showMessage(NodeInterface $node) {

    $completion_rule = NULL;

    $completion_rule_fields = [
      'field_completion_rule_scorm',
      'field_completion_rule_h5p',
      'field_completion_rule_webform',
      'field_completion_rule_discusion'
    ];

    foreach($completion_rule_fields as $field) {
      if ($node->hasField($field) && isset($field)) {
        $completion_rule = $node->$field->value;
      }
    }

    if(isset($completion_rule) && $completion_rule == 0) {
      return FALSE;
    }

    return TRUE;
    
  }

  /**
   * Method description.
   */
  public function messageType($show_next_button) {
    $message_type = 'alert-success';
    if ($show_next_button == 0) {
      $message_type = 'alert-warning';
    }
    return $message_type;
  }

  public function materialHasEnrollments(NodeInterface $node) {
    $has_enrollments = FALSE;
    $material_id = $node->id();
    $course_enrollment = $this->entityTypeManager->getStorage('course_enrollment');
    $course_enrollment_entities = $course_enrollment->loadByProperties(['mid' => $material_id]);
    if (isset($course_enrollment_entities) && !empty($course_enrollment_entities)) {
      $has_enrollments = TRUE;
    }
    return $has_enrollments;
  }

  public function removeCourseEnrollment(NodeInterface $node) {
    $deletion_status = FALSE;
    $deletion_count = 0;
    $material_id = $node->id();

    $course_enrollment = $this->entityTypeManager->getStorage('course_enrollment');
    $course_enrollment_entities = $course_enrollment->loadByProperties(['mid' => $material_id]);
    if (isset($course_enrollment_entities) && !empty($course_enrollment_entities)) {
      foreach($course_enrollment_entities as $enrollment) {
        if ($this->okToDeleteCourseEnrollment($enrollment->id())) {
          $deletion_status[] = $enrollment->id();
          $this->setStatusOfPreviousMaterialToInProgress($node, $enrollment);
          $enrollment->delete();
         
        }
      }
    }

    if ($deletion_status && is_array($deletion_status)) {
      $deletion_count = count($deletion_status);
    }

    return $deletion_count;

  }

  public function setStatusOfPreviousMaterialToInProgress(NodeInterface $node, CourseEnrollmentInterface $course_enrollment) {
    
    $previous_material = $this->courseWrapper->getMaterial($node, -1);
    $uid = $course_enrollment->getOwnerId();
    $sid = $course_enrollment->getSectionId();
    $gid = $course_enrollment->getCourseId();
    $mid = $previous_material->id();

    \Drupal::logger('debug')->debug('uid ' . $uid);
    \Drupal::logger('debug')->debug('sid ' . $sid);
    \Drupal::logger('debug')->debug('gid ' . $gid);
    \Drupal::logger('debug')->debug('mid ' . $mid);

    $storage = $this->entityTypeManager->getStorage('course_enrollment');
    $entities = $storage->loadByProperties([
      'gid' => $gid,
      'sid' => $sid,
      'mid' => $mid,
      'uid' => $uid
    ]);

    if ($entity = current($entities)) {
      $entity->setStatus(CourseEnrollmentInterface::IN_PROGRESS);
      $entity->save();
    }

  }

  protected function okToDeleteCourseEnrollment(int $cid) {
    $ok_to_delete = FALSE;
    $storage = $this->entityTypeManager->getStorage('course_enrollment');
    $course_enrollment = $storage->load($cid);
    if ($course_enrollment instanceof CourseEnrollmentInterface) {
      $section = $course_enrollment->getSection();
      $account = $course_enrollment->getOwner();
      $this->courseWrapper->setCourseFromSection($section);
      if ($course = $this->courseWrapper->getCourse()) {
        \Drupal::logger('debug')->debug('is a course!!!');
        $section_status = $this->courseWrapper->getSectionStatus($section, $account);
        \Drupal::logger('debug')->debug($section_status);
        if ($section_status !== CourseEnrollmentInterface::FINISHED) {
          $ok_to_delete = TRUE;
        }
      }
    }

    return $ok_to_delete;

  }

  public function setNewSectionStatus(int $sid) {
    $course_enrollment = $this->entityTypeManager->getStorage('course_enrollment');
    $course_enrollment_entities = $course_enrollment->loadByProperties(['sid' => $sid]);
    if (isset($course_enrollment_entities) && !empty($course_enrollment_entities)) {
      foreach($course_enrollment_entities as $enrollment) {
        // We also need to set the section to in_progress
        $enrollment->setStatus(1);
        $enrollment->save();
      }
    }
  }

  public function setNewCourseStatus(int $gid) {
    $course_enrollment = $this->entityTypeManager->getStorage('course_enrollment');
    $course_enrollment_entities = $course_enrollment->loadByProperties(['gid' => $gid]);
    if (isset($course_enrollment_entities) && !empty($course_enrollment_entities)) {
      foreach($course_enrollment_entities as $enrollment) {
        // We also need to set the section to in_progress
        $enrollment->setStatus(1);
        $enrollment->save();
      }
    }
  }
  
  public function allowedAttemptsExceeded(NodeInterface $node) {

    $attempts_exceeded = FALSE;

    $this->courseWrapper->setCourseFromMaterial($node);
    if ($course = $this->courseWrapper->getCourse()) {
      $gid = $course->id();
      $section = $this->courseWrapper->getSectionFromMaterial($node);
      $sid = $section->id();
      $mid = $node->id();
      $uid = $this->currentUser->id();        

      // Now look for the course enrollment id
      $storage = $this->entityTypeManager->getStorage('course_enrollment');
      $course_enrollment = $storage->loadByProperties([
        'gid' => $gid,
        'sid' => $sid,
        'mid' => $mid,
        'uid' => $uid
      ]);

      if ($entity = current($course_enrollment)) {
        $status = $entity->getStatus();
        \Drupal::logger('debug')->debug('STATUS!! ' . $status);
        if ($status === CourseEnrollmentCompletionInterface::FAILED) {
          $attempts_exceeded = TRUE;
        }
      }
    }

    return $attempts_exceeded;

  }

  public function getStatusOfMaterial(NodeInterface $node) {

    $show_next_button = FALSE;

    // First check if the material should be hidden
    $this->courseWrapper->setCourseFromMaterial($node);
    if ($course = $this->courseWrapper->getCourse()) {
      $gid = $course->id();
      $section = $this->courseWrapper->getSectionFromMaterial($node);
      $sid = $section->id();
      $mid = $node->id();
      $uid = $this->currentUser->id();

      $storage = $this->entityTypeManager->getStorage('course_enrollment');
      $course_enrollment = $storage->loadByProperties([
        'gid' => $gid,
        'sid' => $sid,
        'mid' => $mid,
        'uid' => $uid
      ]);
      
      if ($entity = current($course_enrollment)) {
        $course_enrollment_id = $entity->id();

        $storage = $this->entityTypeManager->getStorage('course_completion');
        $course_completion = $storage->loadByProperties([
          'cid' => $course_enrollment_id,
          'uid' => $this->currentUser->id()
        ]);

        if ($entity = current($course_completion)) {
          $show_next_button = TRUE;
        }
      }
    }

    return $show_next_button;

  }

}
