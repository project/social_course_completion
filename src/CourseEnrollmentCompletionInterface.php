<?php

namespace Drupal\social_course_completion;

use Drupal\social_course\Entity\CourseEnrollmentInterface;

/**
 * Provides an interface defining a Course Enrollment entity.
 *
 * @ingroup social_course
 * @package Drupal\social_course
 */
interface CourseEnrollmentCompletionInterface extends CourseEnrollmentInterface  {

  const FAILED = 3;

}
