<?php

namespace Drupal\social_course_completion;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a Course Completion entity.
 *
 * @ingroup social_course
 * @package Drupal\social_course_completion
 */
interface CourseCompletionInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  const OPEN = -1;
  const FAILED = 1;
  const COMPLETED = 2;
  

  /**
   * Gets course enrollment object.
   *
   * @return \Drupal\social_course\Entity\CourseEnrollmentInterface
   *   The Group entity.
   */
  public function getCourseEnrollment();

  /**
   * Gets course enrollment id.
   *
   * @return int
   *   The Course enrollment id.
   */
  public function getCourseEnrollmentId();


  /**
   * Gets course completion status.
   *
   * @return int
   *   The CourseCompletion status.
   */
  public function getStatus();

  /**
   * Sets course completion status.
   *
   * @param int $status
   *   Status code.
   *
   * @return \Drupal\social_course_completion\CourseCompletionInterface
   *   The CourseEnrollment entity.
   */
  public function setStatus($status);

  /**
   * Gets course completion score_raw.
   *
   * @return int
   *   The CourseCompletion score_raw.
   */
  public function getScoreRaw();

  /**
   * Sets course completion score_raw.
   *
   * @param int $score_raw
   *   Score raw.
   *
   * @return \Drupal\social_course_completion\CourseCompletionInterface
   *   The CourseCompletion entity.
   */
  public function setScoreRaw($score_raw); 
  
  /**
   * Gets course completion score_max.
   *
   * @return int
   *   The CourseCompletion score_max.
   */
  public function getScoreMax();

  /**
   * Sets course completion score_max.
   *
   * @param int $score_max
   *   Score max.
   *
   * @return \Drupal\social_course_completion\CourseCompletionInterface
   *   The CourseCompletion entity.
   */
  public function setScoreMax($score_max);

  /**
   * Gets course completion score_min.
   *
   * @return int
   *   The CourseCompletion score_min.
   */
  public function getScoreMin();

  /**
   * Sets course completion score_min.
   *
   * @param int $score_min
   *   Score min.
   *
   * @return \Drupal\social_course_completion\CourseCompletionInterface
   *   The CourseCompletion entity.
   */
  public function setScoreMin($score_min);    
  

}
