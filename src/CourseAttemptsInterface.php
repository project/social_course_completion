<?php

namespace Drupal\social_course_completion;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a Course Attempts entity.
 *
 * @ingroup social_course
 * @package Drupal\social_course_completion
 */
interface CourseAttemptsInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets course attempts object.
   *
   * @return \Drupal\social_course\Entity\CourseEnrollmentInterface
   *   The Group entity.
   */
  public function getCourseEnrollment();

  /**
   * Gets course enrollment id.
   *
   * @return int
   *   The Course enrollment id.
   */
  public function getCourseEnrollmentId();
  
  /**
   * Gets course attempts score_raw.
   *
   * @return int
   *   The CourseAttempts score_raw.
   */
  public function getScoreRaw();

  /**
   * Sets course attempts score_raw.
   *
   * @param int $score_raw
   *   Score raw.
   *
   * @return \Drupal\social_course_completion\CourseAttemptsInterface
   *   The CourseAttempts entity.
   */
  public function setScoreRaw($score_raw); 
  
  /**
   * Gets course attempts score_max.
   *
   * @return int
   *   The CourseAttempts score_max.
   */
  public function getScoreMax();

  /**
   * Sets course attempts score_max.
   *
   * @param int $score_max
   *   Score max.
   *
   * @return \Drupal\social_course_completion\CourseAttemptsInterface
   *   The CourseAttempts entity.
   */
  public function setScoreMax($score_max);

  /**
   * Gets course attempts score_min.
   *
   * @return int
   *   The CourseAttempts score_min.
   */
  public function getScoreMin();

  /**
   * Sets course attempts score_min.
   *
   * @param int $score_min
   *   Score min.
   *
   * @return \Drupal\social_course_completion\CourseAttemptsInterface
   *   The CourseCompletion entity.
   */
  public function setScoreMin($score_min);    
  

}
