<?php
/**
 * @file
 * Contains \Drupal\social_course_completion\Controller\SocialCourseCompletionController.
 */

namespace Drupal\social_course_completion\Controller;

use Drupal\social_course\Controller\CoursesController;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Url;
use Drupal\group\Entity\GroupInterface;
use Drupal\node\NodeInterface;
use Drupal\social_course\Entity\CourseEnrollmentInterface;
use Drupal\social_course_completion\CourseEnrollmentCompletionInterface;
use Drupal\social_course\CourseWrapper;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Override the social course controller
 */
class SocialCourseCompletionController extends CoursesController {

  /**
   * {@inheritdoc}
   * Callback of "/group/{group}/section/{node}/start".
   */
  public function startSection(GroupInterface $group, NodeInterface $node) {
    // Get first material.
    $material = $node->get('field_course_section_content')->get(0)->entity;
    $material_redirect = $this->redirect('entity.node.canonical', [
      'node' => $material->id(),
    ]);

    /** @var \Drupal\Core\Entity\EntityStorageInterface $course_enrollment_storage */
    $course_enrollment_storage = $this->entityTypeManager()->getStorage('course_enrollment');
    $entities = $course_enrollment_storage->loadByProperties([
      'uid' => $this->currentUser()->id(),
      'sid' => $node->id(),
    ]);

    // If user has already started or finished section, we just redirect them
    // to the material instead of creating a new enrollment.
    if ($entities) {
      return $material_redirect;
    }

    // Join user to course.
    $storage = $this->entityTypeManager()->getStorage('course_enrollment');
    $storage->create([
      'gid' => $group->id(),
      'sid' => $node->id(),
      'mid' => $material->id(),
      'status' => CourseEnrollmentInterface::IN_PROGRESS,
    ])->save();

    $this->messenger()->addMessage($this->t('You have successfully enrolled'));

    $tags = $group->getCacheTags();
    $tags = Cache::mergeTags($tags, $node->getCacheTags());
    $tags = Cache::mergeTags($tags, $material->getCacheTags());
    Cache::invalidateTags($tags);

    return $material_redirect;
  }

  /**
   * {@inheritdoc}
   * Callback of "/group/{group}/section/{node}/next".
   */
  public function nextMaterial(GroupInterface $group, NodeInterface $node) {

    //$response = parent::nextMaterial($group, $node);

    $storage = $this->entityTypeManager()->getStorage('course_enrollment');
    $field = $node->get('field_course_section_content');
    /** @var \Drupal\social_course\CourseWrapper $course_wrapper */
    $course_wrapper = \Drupal::service('social_course.course_wrapper');
    $course_wrapper->setCourse($group);
    $current_material = NULL;
    $next_material = NULL;
    $account = $this->currentUser();

    foreach ($field->getValue() as $key => $value) {
      $course_enrollment = $storage->loadByProperties([
        'gid' => $group->id(),
        'sid' => $node->id(),
        'mid' => $value['target_id'],
        'uid' => $account->id(),
      ]);

      if (!$course_enrollment) {
        $storage->create([
          'gid' => $group->id(),
          'sid' => $node->id(),
          'mid' => $value['target_id'],
          'status' => CourseEnrollmentInterface::IN_PROGRESS,
        ])->save();
        $next_material = $field->get($key)->entity;
        break;
      }
      elseif (!$current_material) {

        $course_enrollment = current($course_enrollment);

        // Set the correct status for all previous materials.
        // But except self scoring ones
        if ($course_enrollment->getStatus() !== CourseEnrollmentInterface::FINISHED) {
          if ($field->get($key + 1)) {
            $current_material = $field->get($key)->entity;
            $next_material = $field->get($key + 1)->entity;
          }
          else {
            $current_material = $field->get($key)->entity;
          }

          // Read the node id of the material
          $material_id = $course_enrollment->getMaterialId();
          $is_self_scoring_material = $course_wrapper->isSelfScoringMaterial($material_id);
          if (!$is_self_scoring_material) {
            $course_enrollment->setStatus(CourseEnrollmentInterface::FINISHED);
            $course_enrollment->save();
          }
          else {
            if (!$course_wrapper->isSelfScoringMaterialCompleted($course_enrollment->id())) {
              $course_enrollment->setStatus(CourseEnrollmentInterface::IN_PROGRESS);
              $course_enrollment->save();
            }
            else {
              $course_enrollment->setStatus(CourseEnrollmentInterface::FINISHED);
              $course_enrollment->save();
            }
          }
        }
      }
    }

    // Redirect after finishing course.
    if (!$group->get('field_course_redirect_url')->isEmpty()) {
      $uri = $group->get('field_course_redirect_url')->uri;
      $parsed_url = parse_url($uri);

      if (isset($parsed_url['host'])) {
        $response = new TrustedRedirectResponse($uri);
        $response->addCacheableDependency($uri);
      }
      else {
        try {
          $url = Url::fromUri($uri);
        }
        catch (\InvalidArgumentException $exception) {
          $url = Url::fromUserInput($uri);
        }
        $response = new RedirectResponse($url->toString());
      }
    }
    else {
      $response = $this->redirect('entity.group.canonical', [
        'group' => $group->id(),
      ]);
    }

    $redirect_back_to_material = FALSE;

    // Check if we have to redirect back to material
    if (isset($current_material)) {
      if ($course_wrapper->isSelfScoringMaterial($current_material->id())) {
        $conditions = [
          'gid' => $group->id(),
          'sid' => $node->id(),
          'mid' => $current_material->id(),
          'uid' => \Drupal::currentUser()->id()
        ];
        $course_enrollment_id = $course_wrapper->isCourseEnrollment($conditions);
        if ($course_enrollment_id) {
          if (!$course_wrapper->isSelfScoringMaterialCompleted($course_enrollment_id)) {
            $redirect_back_to_material = $current_material->id();
          }
        }
      }
    }

    // Check if user has already seen last material.
    if (!$next_material) {

      // Check if a user has to be redirected to
      // unfinished material
      if ($redirect_back_to_material) {
        $response = $this->redirect('entity.node.canonical', [
          'node' => $redirect_back_to_material,
        ]);
      }
      else {

        $next_section = $course_wrapper->getSection($node, 1);

        if ($next_section) {
          $course_enrollment = $storage->loadByProperties([
            'gid' => $group->id(),
            'sid' => $next_section->id(),
            'uid' => $account->id(),
          ]);
          $course_enrollment = current($course_enrollment);
          $finish_section = !$course_enrollment || $course_enrollment->getStatus() !== CourseEnrollmentInterface::FINISHED;

          if ($finish_section) {
            $this->messenger()->addStatus($this->t('You have successfully finished the @title section', [
              '@title' => $node->label(),
            ]));
          }

          // Redirect to a specific page which set in section.
          $current_section = $course_wrapper->getSection($node, 0);
          if (!$course_wrapper->courseIsSequential() && !$current_section->get('field_course_section_redirect')->isEmpty()) {
            $uri = $current_section->get('field_course_section_redirect')->uri;
            $parsed_url = parse_url($uri);

            if (isset($parsed_url['host'])) {
              $response = new TrustedRedirectResponse($uri);
              $response->addCacheableDependency($uri);
            }
            else {
              try {
                $url = Url::fromUri($uri);
              }
              catch (\InvalidArgumentException $exception) {
                $url = Url::fromUserInput($uri);
              }
              $response = new RedirectResponse($url->toString());
            }
          }
          // Redirect to the next section when it exists and not marked as
          // completed.
          elseif ($finish_section) {
            // Cleanup related entity of "next section".
            $course_enrollment = $storage->loadByProperties([
              'gid' => $group->id(),
              'sid' => $next_section->id(),
              'uid' => $account->id(),
            ]);
            if ($course_enrollment) {
              $storage->delete($course_enrollment);
            }
            $response = self::nextMaterial($group, $next_section);
          }
          // Redirect to next step even if the next step was completed but current
          // material does not have a link of specific next material.
          else {
            $response = $this->redirect('entity.node.canonical', [
              'node' => $next_section->id(),
            ]);
          }

        }
      }
    }
    else {
      if ($redirect_back_to_material) {
        $response = $this->redirect('entity.node.canonical', [
          'node' => $redirect_back_to_material,
        ]);
      }
      else {
        $response = $this->redirect('entity.node.canonical', [
          'node' => $next_material->id(),
        ]);
      }
    }

    $tags = $group->getCacheTags();
    $tags = Cache::mergeTags($tags, $node->getCacheTags());

    if ($next_material) {
      $tags = Cache::mergeTags($tags, $course_wrapper->getMaterial($next_material, -1)->getCacheTags());
    }

    Cache::invalidateTags($tags);


    return $response;
  }
}
