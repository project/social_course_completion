<?php

namespace Drupal\social_course_completion\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\social_course_completion\CourseEnrollmentCompletionInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\social_course_completion\CourseCompletionInterface;

class CourseCompletionStatusForm extends FormBase {

  /**
   * @var AccountInterface $account
   */
  protected $account;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Class constructor.
   */
  public function __construct(AccountInterface $account, EntityTypeManagerInterface $entity_type_manager) {
    $this->account = $account;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('current_user'),
      $container->get('entity_type.manager')
    );
  }

  public function getFormId() {

    // Unique ID of the form.
    return 'course_completion_status_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state, $node = NULL) {

    // Needed to show or hide the next button
    // on the material.
    $show_next_button = 0;
    // Needed to show or hide the material itself
    $exceeded_attempts = 0;
    // Needed for the message
    $course_info_text = NULL;
    // Message type defualt
    $message_type = '';

    if ($node instanceOf NodeInterface) {

      $common_service = \Drupal::service('social_course_completion.common');
      if ($common_service->getStatusOfMaterial($node)) {
        $show_next_button = 1;
      }      

      if ($common_service->allowedAttemptsExceeded($node)) {
        $exceeded_attempts = 1;        
      }      

      // Create a hidden field for our Course Enrollment status.
      $form['show_next'] = array(
        '#type' => 'hidden',
        '#value' => $show_next_button,
      );

      // Create a hidden field for exceeded attempts.
      $form['exceeded_attempts'] = array(
        '#type' => 'hidden',
        '#value' => $exceeded_attempts,
      );

      if ($exceeded_attempts > 0) {
        $course_info_text = t('Sorry! You exceeded the allowed attempts.');
        $message_type = 'alert-warning';
      }
      else {

        // More options to complete now
        $message_from_node = $this->getMessageFromNode($node);
        if ($message_from_node) {
          $course_info_text = t('You have to @field_label!',
            ['@field_label' => $message_from_node]
          );
        }
        // Create a visible information to inform learners
        // what they have to do, or already did!      
        if ($show_next_button == 1) {
          $course_info_text = t('Congratulations! You already finished this!');
        }

        // We only want a message if not view to complete
        $show_message = $common_service->showMessage($node);

        if ($show_message) {
          // We need to know message type
          $message_type = $common_service->messageType($show_next_button);
        }
      }

      // Define message
      $message = [
        '#theme' => 'course_completion_message',
        '#message' => $course_info_text,
        '#message_type' => $message_type
      ];

      $form['course_information'] = [
        '#markup' => render($message),
      ];

    }

    return $form;

  }

  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Get the proper messages based on given node
   *
   * @param NodeInterface $node
   *  The node object.
   * @return string
   *  The field label.
   */
  protected function getMessageFromNode(NodeInterface $node) {

    $field_label = FALSE;

    // H5P
    if ($node->hasField('field_completion_rule_h5p')) {
      $field = 'field_completion_rule_h5p';
      $field_key = $node->field_completion_rule_h5p->value;
    }

    // SCORM
    if ($node->hasField('field_completion_rule_scorm')) {
      $field = 'field_completion_rule_scorm';
      $field_key = $node->field_completion_rule_scorm->value;
    }

    // Discussion
    if ($node->hasField('field_completion_rule_discussion')) {
      $field = 'field_completion_rule_discussion';
      $field_key = $node->field_completion_rule_discussion->value;
    }

    // Discussion
    if ($node->hasField('field_completion_rule_webform')) {
      $field = 'field_completion_rule_webform';
      $field_key = $node->field_completion_rule_webform->value;
    }

    if (isset($field)) {
      $field_config = FieldStorageConfig::loadByName('node', $field);
      $field_label = $field_config->getSettings()['allowed_values'][$field_key];
    }

    return $field_label;

  }

}
