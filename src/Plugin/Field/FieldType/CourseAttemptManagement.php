<?php

namespace Drupal\social_course_completion\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface as StorageDefinition;

/**
 * Plugin implementation of the 'course_attempt_managment' field type.
 *
 * @FieldType(
 *   id = "CourseAttemptManagement",
 *   label = @Translation("Course Attempt Management"),
 *   description = @Translation("Stores course attempt management data."),
 *   category = @Translation("Social course"),
 *   default_widget = "CourseAttemptManagementDefaultWidget",
 *   default_formatter = "CourseAttemptManagementDefaultFormatter"
 * )
 */
class CourseAttemptManagement extends FieldItemBase {

  /**
   * Field type properties definition.
   * 
   * Inside this method we defines all the fields (properties) that our 
   * custom field type will have.
   * 
   * Here there is a list of allowed property types: https://goo.gl/sIBBgO
   */
  public static function propertyDefinitions(StorageDefinition $storage) {

    $properties = [];

    $properties['allowed_attempts'] = DataDefinition::create('integer')
      ->setLabel(t('Allowed attempts'));

    $properties['attempt_rule'] = DataDefinition::create('integer')
      ->setLabel(t('Attempt rule'));

    return $properties;
  }

  /**
   * Field type schema definition.
   * 
   * Inside this method we defines the database schema used to store data for 
   * our field type.
   * 
   * Here there is a list of allowed column types: https://goo.gl/YY3G7s
   */
  public static function schema(StorageDefinition $storage) {

    $columns = [];
    $columns['allowed_attempts'] = [
      'type' => 'int',
      'size' => 'tiny',
    ];
    $columns['attempt_rule'] = [
      'type' => 'int',
      'size' => 'tiny',
    ];

    return [
      'columns' => $columns,
      'indexes' => [],
    ];
  }

  /**
   * Define when the field type is empty. 
   * 
   * This method is important and used internally by Drupal. Take a moment
   * to define when the field fype must be considered empty.
   */
  public function isEmpty() {

    $isEmpty = 
      empty($this->get('allowed_attempts')->getValue()) &&
      empty($this->get('attempt_rule')->getValue());

    return $isEmpty;
  }

}
