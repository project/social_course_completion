<?php

namespace Drupal\social_course_completion\Plugin\Field\FieldWidget;

use Drupal;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'CourseAttemptManagementDefaultWidget' widget.
 *
 * @FieldWidget(
 *   id = "CourseAttemptManagementDefaultWidget",
 *   label = @Translation("Course Attempt Management"),
 *   field_types = {
 *     "CourseAttemptManagement"
 *   }
 * )
 */
class CourseAttemptManagementDefaultWidget extends WidgetBase {

  /**
   * Define the form for the field type.
   * 
   * Inside this method we can define the form used to edit the field type.
   * 
   * Here there is a list of allowed element types: https://goo.gl/XVd4tA
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta, 
    Array $element, 
    Array &$form, 
    FormStateInterface $formState
  ) {

    // allowed_attempts

    $element['allowed_attempts'] = [
      '#type' => 'select',
      '#title' => t('Allowed attempts'),
      '#options' => $this->getAllowedAttemptsOptions(),

      // Set here the current value for this field, or a default value (or 
      // null) if there is no a value
      '#default_value' => isset($items[$delta]->allowed_attempts) ? 
          $items[$delta]->allowed_attempts : 0,

      '#empty_value' => '',
      '#placeholder' => t('Allowed attempts'),
    ];

    // City

    $element['attempt_rule'] = [
      '#type' => 'radios',
      '#options' => $this->getAttemptRuleOptions(),
      '#title' => t('Attempt rule'),
      '#default_value' => isset($items[$delta]->attempt_rule) ? 
          $items[$delta]->attempt_rule : 1,
      '#empty_value' => '',
      '#attributes' => ['class' => ['visually-hidden']]
    ];

    return $element;
  }

  protected function getAllowedAttemptsOptions() {

    $options = [];

    for($i = 0; $i < 20; ++$i) {
      $options[$i] = $i;     
    }

    return $options;

  }

  protected function getAttemptRuleOptions() {

    $options = [
      0 => t('Highest score of attempt'),
      1 => t('Last attempt'),
    ];

    

    return $options;

  }

}
