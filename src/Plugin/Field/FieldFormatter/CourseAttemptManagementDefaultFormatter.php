<?php

namespace Drupal\social_course_completion\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal;

/**
 * Plugin implementation of the 'CourseAttemptManagementDefaultFormatter' formatter.
 *
 * @FieldFormatter(
 *   id = "CourseAttemptManagementDefaultFormatter",
 *   label = @Translation("Course Attempt Management"),
 *   field_types = {
 *     "CourseAttemptManagement"
 *   }
 * )
 */
class CourseAttemptManagementDefaultFormatter extends FormatterBase {

  /**
   * Define how the field type is showed.
   * 
   * Inside this method we can customize how the field is displayed inside 
   * pages.
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'markup',
        '#markup' => t('Allowed attempts/Attempt rule:') . ' ' . $item->allowed_attempts . '/' . $item->attempt_rule
      ];
    }

    return $elements;
  }
  
}
