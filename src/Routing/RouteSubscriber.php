<?php
/**
 * @file
 * Contains \Drupal\social_course_completion\Routing\RouteSubscriber.
 */
 
namespace Drupal\social_course_completion\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('social_course.next_material')) {
      $route->setDefaults([
        '_controller' => '\Drupal\social_course_completion\Controller\SocialCourseCompletionController::nextMaterial',
      ]);
    }
  }
}